$(document).ready(function(){
    $(".set > a").on("click", function(){
        if ($(this).hasClass('active')){
            $(this).removeClass("active");
            $(this).siblings('.content').slideUp(200);
            $(".set > a i").removeClass("fa-minus").addClass("fa-plus");
        }else{
            $(".set > a i").removeClass("fa-minus").addClass("fa-plus");
        $(this).find("i").removeClass("fa-plus").addClass("fa-minus");
        $(".set > a").removeClass("active");
        $(this).addClass("active");
        $('.content').slideUp(200);
        $(this).siblings('.content').slideDown(200);
        }
    });
});

$(document).ready(function(){
    $('ul').click(function(){
        $('ul').toggleClass('active')
        $('section').toggleClass('dark')
        $('.set').toggleClass('dark')
        $('.content p').toggleClass('dark')
        $('.set > a').toggleClass('dark')
        $('.set > a.active').toggleClass('dark')
    })
})