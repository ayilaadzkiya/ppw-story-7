from django.test import TestCase, Client
from .views import Index
from django.urls import resolve

class Story7Test(TestCase):

    def test_Story7_url_is_exist(self):
        response = Client().get('/my-page/')
        self.assertEqual(response.status_code, 200)

    def test_Story7_url_doesnt_exist(self):
        response = Client().get('/random/')
        self.assertEqual(response.status_code, 404)

    def test_Story7_func(self):
        found = resolve('/my-page/')
        self.assertEqual(found.func, Index)

    def test_Story7_to_do_list_template(self):
        response = Client().get('/my-page/')
        self.assertTemplateUsed(response, 'Index7.html')
